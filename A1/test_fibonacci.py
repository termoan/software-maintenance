#CT70A3000 Software Maintenance
#20190119 ATT

import os
import unittest
from fibonacci import Fibonacci
from fibonacci import Handler

class TestFibonacci(unittest.TestCase):
    filename1 = 'test1.txt'
    filename2 = 'test2.txt'
    filename3 = 'test3.txt'

    testValues1 = [2,3,5,8,13]
    testValues2 = [4,6,10,16,26,42,68,110,178,288]
    testValues3 = [10,15]

    def setUp(self):
        self.seq1 = Fibonacci(5,1)
        self.seq2 = Fibonacci(10,2)
        self.seq3 = Fibonacci(2,5)

    @classmethod
    def tearDownClass(cls):
        os.remove(cls.filename1)
        os.remove(cls.filename2)
        os.remove(cls.filename3)

    def test_input(self):
        self.assertEqual(Handler.handleInput('5'), 5)
        self.assertEqual(Handler.handleInput('2', '5'), 2)
        self.assertFalse(Handler.handleInput('asd'))
        self.assertEqual(Handler.handleInput('asd', '2'), 2)

    def test_output(self):
        self.assertListEqual(self.seq1.sequence, self.testValues1)
        self.assertListEqual(self.seq2.sequence, self.testValues2)
        self.assertListEqual(self.seq3.sequence, self.testValues3)

    def test_file(self):
        def readFile(filename):
            testList = []
            file = open(filename, 'r')
            for line in file:
                testList.append(int(line))
            file.close()
            return testList

        self.seq1.writeToFile(self.filename1)
        self.seq2.writeToFile(self.filename2)
        self.seq3.writeToFile(self.filename3)

        testList1 = readFile(self.filename1)
        testList2 = readFile(self.filename2)
        testList3 = readFile(self.filename3)

        self.assertListEqual(testList1, self.testValues1)
        self.assertListEqual(testList2, self.testValues2)
        self.assertListEqual(testList3, self.testValues3)

if __name__ == '__main__':
    unittest.main()
