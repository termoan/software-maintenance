#CT70A3000 Software Maintenance
#20190119 ATT

import os

class Fibonacci:
    def __init__(self, length, startValue):
        self.sequence = []
        self.length = length
        self.startValue = startValue
        self.calculateSequence()

    def calculateSequence(self):
        currentValue = self.startValue
        previousValue = currentValue
        for i in range(0, self.length):
            nextValue = currentValue + previousValue
            previousValue = currentValue
            currentValue = nextValue
            self.sequence.append(currentValue)

    def writeToFile(self, filename):
        file = open(filename, 'w')
        for value in self.sequence:
            file.write(str(value) + '\n')
        file.close()
        print("File <{}\{}> was created.".format(os.getcwd(), filename))

class Handler:
    @staticmethod
    def handleInput(input, default=None):
        try:
            return int(input)
        except ValueError:
            if default:
                return int(default)
            else:
                print("Invalid argument.")
                return False

defaultLength = 10
print("Program that calculates Fibonacci sequence.")
length = input("How long the sequence should be (default {}) : ".format(defaultLength))

while True:
    startValue = input("What number should start the sequence : ")
    if Handler.handleInput(startValue):
        break

seq = Fibonacci(Handler.handleInput(length, defaultLength), Handler.handleInput(startValue))
seq.writeToFile('sequence.txt')
